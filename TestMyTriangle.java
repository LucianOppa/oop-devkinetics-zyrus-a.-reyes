public class TestMyTriangle {
public static void main(String[] args) {
// Test program 
MyTriangle t1 = new MyTriangle( new  MyPoint(0,1), new MyPoint(1,2), new MyPoint(3,4));
MyTriangle t2 = new MyTriangle(0,0,4,8,6,2);
// Testing the overloaded method distance()
System.out.println(t1.toString());
System.out.println(t2.toString());
System.out.println("T1 perimeter = "+t1.getPerimeter());
System.out.println("T2 perimeter = "+t2.getPerimeter());
System.out.println("T1 is "+t1.printType()); // which version? 
System.out.println("T2 is "+t2.printType()); // which version?
}

}