public class MyTime {
public int hour;
public int minute;
public int second;

public void MyTime(int second, int minute, int hour) {
this.hour=hour;
this.minute=minute;
this.second=second;
}

public void setTime(int second, int minute, int hour){
this.hour=hour;
this.minute=minute;
this.second=second;
}

public int getHour() {
return hour;
}

public int getMinute() {
return minute;
}

public int getSecond() {
return second;
}

public void setHour(int hour) {
this.hour=hour;
}

public void setMinute(int minute) {
this.minute=minute;
}
public void setSecond(int second) {
this.second=second;
}

public String nextSecond() {
return hour+":"+minute+":"+(second+1);
}

public String nextMinute() {
return hour+":"+(minute+1)+":"+second;
}

public String nextHour() {
return (1+hour)+":"+minute+":"+second;
}

}