public class MyTriangle{
	public MyPoint v1;
	public MyPoint v2;
	public MyPoint v3;
	public MyTriangle(){
	}
	public MyTriangle(int x1, int y1, int x2, int y2, int x3, int y3){
		v1 = new MyPoint(x1, y1);
		v2 = new MyPoint(x2,y2);
		v3 = new MyPoint(x3, y3);

	}
	public MyTriangle(MyPoint v1, MyPoint v2, MyPoint v3){
		this.v1=v1;
		this.v2=v2;
		this.v3=v3;
	}

	public String toString(){
		return "Triangle @ ("+v1.getX()+","+v1.getY()+"),("+v2.getX()+","+v2.getY()+"),("+v3.getX()+","+v3.getY()+")";
	}
	public double getPerimeter(){
		return v1.distance(v2)+v2.distance(v3)+v3.distance(v1);
	}
	public String printType(){
		int equality =0;
		String type = "unknown";
		if(v1.distance(v2)==v2.distance(v3)){
			equality++;
		}
		if(v1.distance(v2)==v3.distance(v1)){
			equality++;
		}
		if(v2.distance(v3)==v1.distance(v3)) equality++;

		switch(equality){
			case 0: 
				type = "scalene";
			break;
			case 1:
				type = "isosceles";
			break;
			case 3:
				type = "equilateral";
			break;

			default: break;
		
		}
		return type;
	}

}