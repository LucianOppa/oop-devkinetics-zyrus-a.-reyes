public class TestAuthor {
public static void main(String[] args) {
Author anAuthor = new Author("Tan Ah Teck", "ahteck@somewhere.com", 'm');
System.out.println(anAuthor.toString()); // call toString()
anAuthor.setEmail("paul@nowhere.com");
System.out.println(anAuthor.toString());

Author bookAuthor = new Author("Zyrus A. Reyes", "reyes_zyrus_it@yahoo.com.ph", 'm');
Book aBook = new Book();
aBook.Book("Java for dummy", bookAuthor, 19.95, 1000);
System.out.println(aBook.toString());
//System.out.println(aBook.getAuthor());


System.out.print("Book Title :"+aBook.getName());
System.out.print("\nAuthor :"+aBook.getAuthorName());
System.out.print("\nAuthor E-mail :"+aBook.getAuthorEmail());
System.out.print("\nAuthor Gender :"+aBook.getAuthorGender());
}
}