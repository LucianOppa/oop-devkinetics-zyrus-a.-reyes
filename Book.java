public class Book{
public String name;
public Author author = new Author();
public double price;
public int qtyInStock;


public void Book(String name, Author author, double price) {
this.name = name;
this.author = author;
this.price = price;
}

public void Book(String name, Author author, double price, int qtyInStock) {
this.name = name;
this.author = author;
this.price = price;
this.qtyInStock = qtyInStock;
}

public String getName() {
return name;
}

public Author getAuthor() {
return author;
}

public double getPrice() {
return price;
}

public int getQtyInStock() {
return qtyInStock;
}

public void setQtyInStock(int qtyInStock) {
this.qtyInStock=qtyInStock;
}

public void setPrice(double price) {
this.price=price;
}

public String toString() {
return name+" by "+author;
}

public String getAuthorName() {
return author.getName();
}

public String getAuthorEmail() {
return author.getEmail();
}

public char getAuthorGender() {
return author.getGender();
}

}