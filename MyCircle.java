public class MyCircle {
public MyPoint center = new MyPoint();
public int radius = 2;
public int x;
public int y;

public void MyCircle(int x, int y, int radius) {
this.x=x;
this.y=y;
this.radius=radius;
}

public MyPoint getCenter(MyPoint center) {
this.center = center;
return this.center;
}
 
public void MyCircle(MyPoint center, int radius) {
this.center=center;
this.radius=radius;
}

public int getRadius() {
return radius;
}

public void setRadius(int radius) {
this.radius=radius;
}

public MyPoint getCenter() {
return center;
}

public void setCenter(MyPoint center) {
this.center=center;
}

public int getCenterX() {
return x;
}

public int getCenterY() {
return y;
}

public void setCenterXY(int x, int y) {
this.x=x;
this.y=y;
}

public String toString() {
return "x: "+x+" y: "+y;
}

public double getArea() {
return (radius*radius)*Math.PI;
}
}