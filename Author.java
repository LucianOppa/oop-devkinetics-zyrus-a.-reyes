public class Author {
public String name;
public String email;
public char gender;

public Author() {
name = "Zyrus A. Reyes";
email = "reyes_zyrus_it@yahoo.com";
gender = 'm';
}

public Author( String name, String email, char gender) {
this.name = name;
this.email = email;
this.gender = gender;
}

public String getName() {
return name;
}

public String getEmail() {
return email;
}

public char getGender() {
return gender;
}

public void setEmail(String email) {
this.email=email;
}

public String toString() {
return "Name "+name+" email "+email+" gender "+gender;
}
}